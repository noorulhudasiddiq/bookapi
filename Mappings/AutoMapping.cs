﻿using AutoMapper;
using BookAPI.DTO;
using BookAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.Mappings
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            this.CreateMap<Country, CountryDTO>();
            this.CreateMap<Category, CategoriesDTO>();

            this.CreateMap<Book, BookDTO>();
            this.CreateMap<Author, AuthorDTO>();
            this.CreateMap<Review, ReviewDTO>();
            this.CreateMap<Reviewer, ReviewerDTO>();
        }
    }
}
