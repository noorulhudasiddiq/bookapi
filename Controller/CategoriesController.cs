﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BookAPI.DTO;
using BookAPI.Repositories.IRepositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BookAPI.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;

        public CategoriesController(ICategoryRepository categoryRepository, IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(400)]
        [ProducesResponseType(200, Type = typeof(IEnumerable<CategoriesDTO>))]
        public IActionResult GetCategories()
        {
            var categories = _categoryRepository.GetCategories();

            if (!ModelState.IsValid)
                BadRequest(ModelState);

            var categoriesDto = new List<CategoriesDTO>();

            foreach(var category in categories)
            {
                categoriesDto.Add(_mapper.Map<CategoriesDTO>(category));
            }
            return Ok(categoriesDto);
        }

        [HttpGet("{categoryId}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(CategoriesDTO))]
        public IActionResult GetCategoryById(int categoryId)
        {
            if (!_categoryRepository.CategoryExist(categoryId))
                NotFound();

            var category = _categoryRepository.GetCategoryById(categoryId);

            if (!ModelState.IsValid)
                BadRequest(ModelState);

            var categoryDto = _mapper.Map<CategoriesDTO>(category);

            return Ok(categoryDto);
        }

        [HttpGet("{categoryId}/books")]
        [ProducesResponseType(400)]
        [ProducesResponseType(200, Type = typeof(IEnumerable<BookDTO>))]

        public IActionResult GetBooksOfACategory(int categoryId)
        {
            if (!_categoryRepository.CategoryExist(categoryId))
                NotFound();

            var books = _categoryRepository.GetBooksOfACategory(categoryId);

            var bookDtos = new List<BookDTO>();
            foreach( var book in books)
            {
                bookDtos.Add(_mapper.Map<BookDTO>(book));
            }

            return Ok(bookDtos);
        }

        [HttpGet("books/{bookId}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(200, Type =typeof(IEnumerable<CategoriesDTO>))]
        public IActionResult GetCategoriesOfABook(int bookId)
        {
            var categories = _categoryRepository.GetCategoriesOfABook(bookId);

            if (!ModelState.IsValid)
                BadRequest();

            var categoryDtos = new List<CategoriesDTO>();
            foreach( var category in categories)
            {
                categoryDtos.Add(_mapper.Map<CategoriesDTO>(category));
            }

            return Ok(categoryDtos);
        }
    }
}