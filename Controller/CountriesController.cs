﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BookAPI.DTO;
using BookAPI.Models;
using BookAPI.Repositories.IRepositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BookAPI.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class CountriesController : ControllerBase
    {
        ICountryRepository _countryRepository;
        private readonly IMapper _mapper;
        public CountriesController(ICountryRepository countryRepository, IMapper mapper)
        {
            _countryRepository = countryRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(400)]
        [ProducesResponseType(200, Type= typeof(IEnumerable<CountryDTO>))]
        public IActionResult GetCountries()
        {
            var countries  = _countryRepository.GetCountries();
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var countriesDTO = new List<CountryDTO>();
            foreach(var countrydto in countries)
            {
                countriesDTO.Add(_mapper.Map<CountryDTO>(countrydto));
            }
            

            return Ok(countriesDTO);
        }

        [HttpGet("{countryId}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(CountryDTO))]
        public IActionResult GetCountryById(int countryId)
        {
            if (!_countryRepository.CountryExist(countryId))
                NotFound();

            var country = _countryRepository.GetCountryById(countryId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var countryDTO = new CountryDTO();
           
                countryDTO  = _mapper.Map<CountryDTO>(country);

            return Ok(countryDTO);
        }

        [HttpGet("authors/{authorId}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(CountryDTO))]
        public IActionResult GetCountryOfAnAuthor(int authorId)
        {

            var country = _countryRepository.GetCountryOfAnAuthor(authorId);

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var countryDTO = new CountryDTO();

            countryDTO = _mapper.Map<CountryDTO>(country);

            return Ok(countryDTO);
        }

        [HttpGet("{countryId}/authors")]
        [ProducesResponseType(400)]
        [ProducesResponseType(200, Type= typeof(IEnumerable<AuthorDTO>))]

        public IActionResult GetAuthorsFromACountry(int countryId)
        {
            if (!_countryRepository.CountryExist(countryId))
                NotFound();

            var authors = _countryRepository.GetAuthorsFromCountry(countryId);

            if (!ModelState.IsValid)
                BadRequest();

            var authorDto = new List<AuthorDTO>();
            
            foreach(var author in authors)
            {
                authorDto.Add(_mapper.Map<AuthorDTO>(author));
            }

            return Ok(authorDto);
        }
    }
}