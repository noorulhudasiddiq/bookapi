﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BookAPI.DTO;
using BookAPI.Repositories.IRepositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BookAPI.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly IBookRepository _bookRepository;
        private readonly IMapper _mapper;

        public BookController(IBookRepository bookRepository, IMapper mapper)
        {
            _bookRepository = bookRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(400)]
        [ProducesResponseType(200, Type = typeof(IEnumerable<BookDTO>))]

        public IActionResult GetBooks()
        {
            var books = _bookRepository.GetBooks();

            if (!ModelState.IsValid)
                BadRequest();

            var booksDto = new List<BookDTO>();

            foreach (var book in books)
            {
                booksDto.Add(_mapper.Map<BookDTO>(book));
            }

            return Ok(booksDto);
        }

        [HttpGet("{bookId}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(BookDTO))]

        public IActionResult GetBookById(int bookId)
        {
            if (!_bookRepository.BookExists(bookId))
                NotFound();

            var book = _bookRepository.GetBook(bookId);

            if (!ModelState.IsValid)
                BadRequest();

            var booksDto = _mapper.Map<BookDTO>(book);

            return Ok(booksDto);
        }

        [HttpGet("ISBN/{bookIsbn}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(BookDTO))]

        public IActionResult GetBookByIsbn(string bookIsbn)
        {
            if (!_bookRepository.BookExists(bookIsbn))
                NotFound();

            var book = _bookRepository.GetBook(bookIsbn);

            if (!ModelState.IsValid)
                BadRequest();

            var booksDto = _mapper.Map<BookDTO>(book);

            return Ok(booksDto);
        }

        [HttpGet("rating/{bookId}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(decimal))]

        public IActionResult GetBookRating(int bookId)
        {
            if (!_bookRepository.BookExists(bookId))
                NotFound();

            var bookRating = _bookRepository.GetBookRating(bookId);

            if (!ModelState.IsValid)
                BadRequest();

            return Ok(bookRating);
        }
    }
}