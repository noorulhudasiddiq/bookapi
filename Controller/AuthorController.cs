﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using BookAPI.DTO;
using BookAPI.Repositories.IRepositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BookAPI.Controller
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly IAuthorRepository _authorRepository;
        private readonly IMapper _mapper;

        public AuthorController(IAuthorRepository authorRepository, IMapper mapper)
        {
            _authorRepository = authorRepository;
            _mapper = mapper;
        }

        [HttpGet]
        [ProducesResponseType(400)]
        [ProducesResponseType(200, Type =typeof(IEnumerable<AuthorDTO>))]

        public IActionResult GetAuthors()
        {
            var authors = _authorRepository.GetAuthors();

            if (!ModelState.IsValid)
                BadRequest();

            var authorsDto = new List<AuthorDTO>();

            foreach(var author in authors)
            {
                authorsDto.Add(_mapper.Map<AuthorDTO>(author));
            }

            return Ok(authorsDto);
        }

        [HttpGet("{authorId}")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(AuthorDTO))]

        public IActionResult GetAuthorById(int authorId)
        {
            if (!_authorRepository.AuthorExists(authorId))
                NotFound();

            var author = _authorRepository.GetAuthor(authorId);

            if (!ModelState.IsValid)
                BadRequest();

            var authorDto  = _mapper.Map<AuthorDTO>(author);
        
            return Ok(authorDto);
        }

        [HttpGet("book/{bookId}/authors")]
        [ProducesResponseType(400)]
        [ProducesResponseType(200, Type = typeof(IEnumerable<AuthorDTO>))]

        public IActionResult GetAuthorsOfABook(int bookId)
        {
            var authors = _authorRepository.GetAuthorsOfABook(bookId);

            if (!ModelState.IsValid)
                BadRequest();

            var authorsDto = new List<AuthorDTO>();

            foreach (var author in authors)
            {
                authorsDto.Add(_mapper.Map<AuthorDTO>(author));
            }

            return Ok(authorsDto);
        }

        [HttpGet("{authorId}/books")]
        [ProducesResponseType(400)]
        [ProducesResponseType(404)]
        [ProducesResponseType(200, Type = typeof(IEnumerable<BookDTO>))]

        public IActionResult GetBooksOfAnAuthor(int authorId)
        {
            if (!_authorRepository.AuthorExists(authorId))
                NotFound();

            var books = _authorRepository.GetBooksOfAnAuthor(authorId);

            if (!ModelState.IsValid)
                BadRequest();

            var booksDto = new List<BookDTO>();

            foreach (var book in books)
            {
                booksDto.Add(_mapper.Map<BookDTO>(book));
            }

            return Ok(booksDto);
        }
    }
}