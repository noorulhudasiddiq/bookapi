﻿using BookAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.Repositories.IRepositories
{
    public interface ICountryRepository
    {
        ICollection<Country> GetCountries();
        Country GetCountryById(int countryId);
        ICollection<Author> GetAuthorsFromCountry(int countryId);
        Country GetCountryOfAnAuthor(int authorId);
        bool CountryExist(int countryId);
    }
}
