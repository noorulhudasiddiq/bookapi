﻿using BookAPI.Models;
using BookAPI.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.Repositories.Repositories
{
    public class CountryRepository : ICountryRepository
    {

        BookDbContext _countryDbContext;

        public CountryRepository(BookDbContext countryDbContext)
        {
            _countryDbContext = countryDbContext;
        }

        public bool CountryExist(int countryId)
        {
            return _countryDbContext.Countries.Any(c=> c.Id == countryId);
        }

        public ICollection<Author> GetAuthorsFromCountry(int countryId)
        {
            return _countryDbContext.Authors.Where(c => c.Id == countryId).ToList();
        }

        public ICollection<Country> GetCountries()
        {
            return _countryDbContext.Countries.OrderBy(c => c.Name).ToList();
        }

        public Country GetCountryById(int countryId)
        {
            return _countryDbContext.Countries.Where(c => c.Id == countryId).FirstOrDefault();
        }

        public Country GetCountryOfAnAuthor(int authorId)
        {
            return _countryDbContext.Authors.Where(a => a.Id == authorId).Select(c => c.Country).FirstOrDefault();
        }
    }
}
