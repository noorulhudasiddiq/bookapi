﻿using BookAPI.Models;
using BookAPI.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.Repositories.Repositories
{
    public class AuthorRepository : IAuthorRepository
    {

        BookDbContext _authorDbContext;

        public AuthorRepository(BookDbContext authorDbContext)
        {
            _authorDbContext = authorDbContext;
        }
        public bool AuthorExists(int authorId)
        {
            return _authorDbContext.Authors.Any(a => a.Id == authorId);
        }

        public Author GetAuthor(int authorId)
        {
            return _authorDbContext.Authors.Where(a => a.Id == authorId).FirstOrDefault();
        }

        public ICollection<Author> GetAuthors()
        {
            return _authorDbContext.Authors.OrderBy(a => a.FirstName).ToList();
        }

        public ICollection<Author> GetAuthorsOfABook(int bookId)
        {
            return _authorDbContext.BoookAuthors.Where(b => b.BookId == bookId).Select(a => a.Author).ToList();
        }

        public ICollection<Book> GetBooksOfAnAuthor(int authorId)
        {
            return _authorDbContext.BoookAuthors.Where(a => a.AuthorId == authorId).Select(b => b.Book).ToList();
        }
    }
}
