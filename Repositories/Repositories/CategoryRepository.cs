﻿using BookAPI.Models;
using BookAPI.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.Repositories.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly BookDbContext _categoryDbContext;

        public CategoryRepository(BookDbContext categoryDbContext)
        {
            _categoryDbContext = categoryDbContext;
        }

        public bool CategoryExist(int categoryId)
        {
            return _categoryDbContext.Categories.Any(c=> c.Id == categoryId);
        }

        public ICollection<Book> GetBooksOfACategory(int categoryId)
        {
            return _categoryDbContext.BookCategories.Where(c => c.CategoryId == categoryId).Select(b => b.Book).ToList();
        }

        public ICollection<Category> GetCategories()
        {
            return _categoryDbContext.Categories.OrderBy(c => c.Name).ToList();
        }

        public ICollection<Category> GetCategoriesOfABook(int bookId)
        {
            return _categoryDbContext.BookCategories.Where(b => b.BookId == bookId).Select(c => c.Category).ToList();
        }

        public Category GetCategoryById(int categoryId)
        {
            return _categoryDbContext.Categories.Where(c => c.Id == categoryId).FirstOrDefault();
        }
    }
}
