﻿using BookAPI.Models;
using BookAPI.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.Repositories.Repositories
{
    public class ReviewerRepository : IReviewerRepository
    {
        BookDbContext _reviewerDbContext;

        public ReviewerRepository( BookDbContext reviewerDbContext)
        {
            _reviewerDbContext = reviewerDbContext;

        }
        public Reviewer GetReviewer(int reviewerId)
        {
            return _reviewerDbContext.Reviewers.Where(r => r.Id == reviewerId).FirstOrDefault();
        }

        public Reviewer GetReviewerOfAReview(int reviewId)
        {
            var reviewerId = _reviewerDbContext.Reviews.Where(r => r.Id == reviewId).Select(re => re.Reviewer.Id).FirstOrDefault();
            return _reviewerDbContext.Reviewers.Where(re => re.Id == reviewerId).FirstOrDefault();
        }

        public ICollection<Reviewer> GetReviewers()
        {
            return _reviewerDbContext.Reviewers.OrderBy(r => r.FirstName).ToList(); 
        }

        public ICollection<Review> GetReviewsByReviewer(int reviewerId)
        {
            return _reviewerDbContext.Reviews.Where(r => r.Reviewer.Id == reviewerId).ToList();
        }

        public bool ReviewerExists(int reviewerId)
        {
            return _reviewerDbContext.Reviewers.Any(r => r.Id == reviewerId);
        }
    }
}
