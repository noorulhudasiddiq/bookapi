﻿using BookAPI.Models;
using BookAPI.Repositories.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookAPI.Repositories.Repositories
{
    public class ReviewRepository : IReviewRepository
    {
        BookDbContext _reviewDbContext;

        public ReviewRepository(BookDbContext reviewDbContext)
        {
            _reviewDbContext = reviewDbContext;
        }
             
        public Book GetBookOfAReview(int reviewId)
        {
            return _reviewDbContext.Reviews.Where(r => r.Id == reviewId).Select(b => b.Book).FirstOrDefault();
        }

        public Review GetReview(int reviewId)
        {
            return _reviewDbContext.Reviews.Where(r => r.Id == reviewId).FirstOrDefault();
        }

        public ICollection<Review> GetReviews()
        {
            return _reviewDbContext.Reviews.OrderBy(r => r.Rating).ToList();
        }

        public ICollection<Review> GetReviewsOfABook(int bookId)
        {
            return _reviewDbContext.Reviews.Where(r => r.Book.Id == bookId).ToList() ;
        }

        public bool ReviewExists(int reviewId)
        {
            return _reviewDbContext.Reviews.Any(r => r.Id == reviewId);
        }
    }
}
